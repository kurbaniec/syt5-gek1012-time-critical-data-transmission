# Embedded Devices "Time-critical data-transmission"

## Aufgabenstellung
Die detaillierte [Aufgabenstellung](TASK.md) beschreibt die notwendigen Schritte zur Realisierung.

## Recherche

#### Was ist ein SPI-Bus und wie ist dieser aufgebaut?

SPI (Serial Peripheral Interface Bus) wird zur Kurzdistanzkommunikation zwischen mehreren Geräten verwendet. Die Architektur besteht zumeist aus einem Master und mehreren Slaves, wobei allerdings auch andere Architekturen möglich sind. [1, 2, 3]

Kaskadierung der Slaves [3]:

![img](https://upload.wikimedia.org/wikipedia/commons/thumb/9/97/SPI_three_slaves_daisy_chained.svg/330px-SPI_three_slaves_daisy_chained.svg.png)

SPI-Sternverbindung [3]:

![img](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/SPI_three_slaves.svg/330px-SPI_three_slaves.svg.png)

##### SPI-Signale [2]:

Master Out, Slave In (MOSI) - Kommunikationskanal vom Master zum Slave

Master In, Slave Out (MISO) - Kommunikationskanal vom Slave zum Master

Serial Clock (SCK) - Dient der Synchronisation zwischen Master und Slave

Slave Select (SS) - Dient der Auswahl des Client Devices

###### Default RaspberryPi GPIO Pins used for SPI [1]

| Board Pin | BCM Number | Function                   |
| --------- | ---------- | -------------------------- |
| 19        | GPIO 10    | MOSI - Master Out Slave In |
| 21        | GPIO 9     | MISO - Master In Slave Out |
| 23        | GPIO 11    | SCLK - Serial Clock        |
| 24        | GPIO 8     | CE0 - Chip Select 0        |
| 26        | GPIO 7     | CE1 - Chip Select 1        |

#### Welche Vorteile ergeben sich bei der Verwendung eines Kommunikationsbusses?

Kommunikationsbusse sind platformübergreifend, daher können sind von einer Vielzahl von verschiedenen Geräten zur Kommunikations verwendet werden. Darüber hinaus ist die Pinbelegung meist sehr gering, was wiederum die Komplexität des System veringert und selbst die einfachsten Sensoren mit einschließt. SPI hat darüber den Vorteil vollduplexfähig zu sein, das bedeutet, dass in beide Richtungen gleichzeitig kommuniziert werden kann.

#### Welche Möglichkeiten der Beschaltung sind beim SPI-Bus möglich und wie wirkt sich die Clock darauf aus?

Die standardmäßige Beschaltung des SPI-Busses sieht wie folgt aus:

Sobald der Slave-Select herunterschaltet wird die Kommunikation gestartet. Dabei werden die Bits vom Master in den Slave geshifted (MOSI) und vom Slave in den Master (MISO). Dieser beidseitige Vorgang findet auch bei nur einseitiger Informationsübertragung statt, beispielsweise  werden einfache Taster keine Informationen von ihrem Master erhalten sondern lediglich eine Antwort zurück geben. Die Clock signalisiert die Geschwindigkeit in der die einzelnen Bits die übertragen werden sollen.[4]

![img](https://www.allaboutcircuits.com/uploads/articles/Hughes_TechnicalArticles_SPI51.gif)

Die Clock im SPI Bus ist auf 2 Parameter angewiesen, der Clock Polarity (CPOL) und der Clock Phase (CPHA). Die Clock Polarity beschreibt den Standardzustand der Clock, den sie einnimmt wenn keine Übertragung stattfindet. Bei einer CPOL von 0 beginnt , wie im Beispiel davor, jeder Impuls mit einer steigenden Flanke und endet mit einer sinkenden bis der Basiszustand von 0 wieder erreicht wird. Bei einer CPOL von 1 sind die Schwingungen genau umgekehrt. Die Clock Phase beschreibt wann die eigentliche Datenübertragung stattfindet und wie lange die Information gehalten werden muss. Bei einer CPHA von 0 werden die Daten am OUT bei der "trailing edge", sobald die Clock wieder ihren Ursprungszustand erreicht hat, versendet und kommen am IN bei der "leading edge" an, wenn eine neue Clock Flanke gestartet wird. Bei einer CPHA von 1 verhält sich die Datenübertragung erneut umgekehrt, bei einer "leading edge" werden die Informationen am OUT Pin freigegeben und bis zur "trailing edge" gehalten, damit der IN Pin sie erhält. [3]

![img](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/SPI_timing_diagram2.svg/400px-SPI_timing_diagram2.svg.png)

Rote Linie: "leading edge"

Blaue Linie: "trailing edge"

#### Wie werden zeitkritische Anwendungen (real-time) eingeteilt?

* Hard / Hart

  Ergebnisse müssen immer innerhalb eines Zeitrahmens geliefert werden,  eine Überschreitung wird als System-Versagen interpretiert.

* Soft / Weich

  Ergebnisse werden meist innerhalb eines Zeitrahmens geliefert, eine Überschreitung kann aber vorkommen. Weiters verlieren die Ergebnisse mit der Zeit ihren Nutzen, weshalb eine Verspätung die Qualität eines Services degradiert.

* Firm / Fest

  Ergebnisse werden innerhalb eines Zeitrahmens, oder gar nicht geliefert. Bei festen Echtzeitsystem droht kein unmittelbarer Schaden durch fehlende Ergebnisse, denn diese werden bei Überschreiten der Zeitanforderungen verworfen, da sie nutztlos für das System sind.

[5]

#### Wie kommt ein Watchdog bei zeitkritischen Anwendungen zum Einsatz?

Falls ein Programm hängt oder eine Hardware Probleme macht, hilft es oft auf den Reset Knopf zu drücken oder die Stromversorgung zu kappen. Bei Alltagsproblemen ist dies schnell und einfach gelöst, doch man kann beispielsweise nicht händisch große Serverlandschaften manuell in Stande halten. 

Ein Watchdog macht nichts anderes als automatisch den Reset-Knopf zu drücken.  Dabei drückt der Watchdog nicht wirklich den Reset-Knopf, er sendet einfach das gleiche Signal. Ein Watchdog kann unterschiedlich implementiert werden, arbeitet aber immer ähnlich. Das Gerät, das beobachtet wird, sendet zyklisch an den Watchdog ein Signal, welches den Reset-Vorgang des Watchdogs immer wieder aufschiebt. Bekommt der Watchdog kein Signal mehr, dann wird der Reset nach Ablauf der Zeitanforderungen vollzogen. [6]

#### Wie kann man Interrupts priorisieren?

Prioritäten bei Interrupts sind dann wichtig, wenn man mehrere Interrupts hat und diese konmtrollieren/steuern möchte. Vergibt man keine Interrupt-Priorität, dann haben alle Interrupts die gleiche (niedrigste) Priorität. Nur ein Interrupt mit einer hohen Prioritätsstufe, kann einen Interrupt mit einer niedrigeren Prioritätsstufe unterbrechen. [7]

Wie Interrupts priosiert werden, hängt vom verwendeten Chipsatz ab. Bei einem Mikrocontroller mit einem Arm Cortex-M ist die Priosierung in einem Register des NVIC (Nested Vecotred Interrupt Controller) definiert,  welcher die Interrupts im Programm dann auch konkret priorisiert. [8], [9]

Doch wieso sollte man Sachen überhaupt priosieren? Wenn beispielsweise ein externes Event schnell verarbeitet werden muss, was vor allem bei sicherheitsrelevanten Anlagen wichtig ist, dann kann das nur durch eine Unterbrechung des Hauptprogrammes und konkrete Interrupt-Anweisung effizient gelöst werden. Auch benutzen viele Geräte eine Interrupt Routine, um aus dem "Sleep"-Mode wieder aufzuwachen und Strom zu sparen. Auch Watchdogs basieren auf Interrupts, ein Watchdog kann nicht warten wenn es zu Fehlern kommt, er muss sofort handeln. [10]

#### Was sind Real-Time Operating-Systems (RTOS) und wie kann man diese auf Mikrokontrollern einsetzen?

Ein Echzeitbetriebssystem ist eine Software, die die Zeit eines Mikroprozessors oder Mikrocontrollers verwarltet. Auch bietet ein RTOS eine Multitasking-Umgebung, in der mehrere Dinge jeweils gleichzeitig ablaufen. Dazu zerlegt es ein Programm in mehrere Tasks. Diese Maßnahme gaukelt dem System vor, es stünden mehrere CPUs zur Verfügung. Neben dem Multitasking bietet ein Echtzeitbetriebssystem auch weitere wertvolle Dienste: Verzögerungen, der Schutz gemeinsam genutzter Ressourcen und eine Kommunikation zwischen einzelnen Tasks sowie eine Synchronisation gehören zu dessen typischen Funktionen. [11]

Ein Task ist ein Stück Programmcode, das »denkt«, ihm stehe die CPU allein zur Verfügung. Dieser Code kann eigenständig sein oder Schnittstellen zu anderer Software im System besitzen (z.B. TCP/IP, USB, ..). [11]

Weiters besitzten Tasks Zustände, die ihren aktuellen Status beschreiben:

* **Blockiert** - der Task wartet auf den Eintritt eines Ereignisses oder den Ablauf eines Timeouts.

* **Bereit** (ready) - der Task ist bereit zur Ausführung und wartet darauf, dass ihn das Echtzeit-betriebssystem für die Ausführung durch die CPU in den Schedule übernimmt.

* **Aktiv** (running) - der Task wird aktuell auf der CPU ausgeführt; es kann sich jeweils immer nur ein Task in diesem Zustand befinden.

[11]

## Manjaro Setup

Die folgenden Pakete müssen heruntergeladen werden um das Template verwenden zu können.

```
pacman -Syyu arm-none-eabi-gcc arm-none-eabi-newlib arm-none-eabi-binutils stlink cmake
```

## Raspberry Setup

Um SPI am Rasbberry Pi zu aktivieren muss man den Kommentar beim Parameter `dtparam=spi=on` in `/boot/config.txt` entfernen und den Raspberry anschließend neustarten.

Nach dem Neustart sollte die SPI-Schnittstelle des Raspberries aktiv sein. Mit `lsmod | grep "spidev "` bzw. `ls /dev | grep spi` kann getestet werden ob der Vorgang funktioniert hat.

Die GPIO-Pins 12, 13, 14, 10 und 11 sind für SPI am Raspberry vorgesehen.

![](img/README/raspy_pins.PNG)

## Logic-Analyzer

Mithifle eines Logik-Analyzers kann man den Zeitverlauf eines digitalen Signales aufzeichen und bildlich darstellen. Damit kann man Fehler beispielsweise beim Programmieren eines Mikrocontrollers aufspüren bzw. testen ob alles ordnungsmäßig funktioniert. Mithilfe eines Logic-Analyzer konnten wie die SPI-Kommunkation zwischen Microcontroller und Raspberry analysieren und Fehlerfälle, wie eine nicht ordnungsmäßige funktionierende Clock, am Raspberry finden. Dazu haben wir die Logic-Analyzer-Software von Saleae verwendet, die ein Profil für SPI besitzt.

[12, 13]

![](img/README/logic-analyser.png)

## Implementierung - Ampelsteuerung ohne SPI

Eine generelle Ampelsteuerung ohne SPI befindet sich im Projekt `ampel`. Folgender Befehl lädt das Programm auf den STM32F4:

```
make clean flash PROJ=src/ampel
```

## Implementierung - Ampelsteuerung mit SPI

Die Ampelsteuerung befindet sich im Projekt `spi_ampel`. Folgender Befehl lädt das Programm auf den STM32F4:

```
make clean flash PROJ=src/spi_ampel
```

### Durchführung und Testing

1. ```
   make clean flash PROJ=src/spi_ampel
   ```

2. Betätigen des BLAUEN Tasters

Sobald das Programm erfolgreich auf den STM32F4 geflasht wurde, sollten alle 4 LEDs (Rot, Gelb, Grün, Blau) schnell blinken. Daraufhin wartet das Programm bis die Ampelsteuerung über den blauen Taster gestartet wird, wichtig dafür ist das Anschließen eines geeigneten SPI-Empfängers. Sobald eine erfolgreiche Kommunikation stattfindet sollten die LEDs am Board die Ampelsteuerung darstellen und über SPI die Status-Änderungen übertragen werden.

Sobald eine Übertragung fehl schlägt leuchtet die blaue LED auf, so lange bis wieder eine erfolgreiche Kommunikation stattgefunden hat und das Programm die blaue LED wieder deaktiviert.

### Aufbau des Source-Codes - STM32F4

Der Sourcecode für das Projekt Ampelsteuerung mit SPI basiert auf den Beispielen `SPI_FullDuplex_ComIT` und `TIM_TimeBase`, ebenfalls angeführt in diesem Projekt. Der Sourcecode gliedert sich in zwei Teile der Kommunikation über den SPI-Bus und den Timer Interrupts, welche alle 100ms aufgerufen werden. Die eigentliche Ampelsteuerung befindet sich im File `main.c`. Alle anderen Files beinhalten lediglich Konfigurationen, welche zu Begin der main-Methode mit einbezogen werden.

#### Ampelabfolge

Folgender Code erhält die allgemeine Ampelsteuerung.  Die Variable `error_flag`, beschreibt den derzeitigen Zustand der Ampel. Da ein direktes Umschalten zwischen Alarmmodus und regulärem Ampelbetrieb ermöglicht werden soll, wird nach jeder längeren Wartepause überprüft ob mittlerweile die `error_flag` gesetzt wurde. Die Variable `current_state` speichert den derzeitigen Ampelzustand und wird in weiterer Folge über den Timer Interrupt regelmäßig über den SPI-Bus an den Slave gesendet.

```c
while (1)
    {
		while (!error_flag)
		{
			current_state = RED;
			BSP_LED_On(LED5);
			HAL_Delay(3000);
			
			if (error_flag) {
				break;
			}
			
			current_state = RED_YELLOW;
			BSP_LED_On(LED3);
			HAL_Delay(1000);
			
			if (error_flag) {
				break;
			}
			
			BSP_LED_Off(LED3);
			BSP_LED_Off(LED5);
			current_state = GREEN;
			BSP_LED_On(LED4);
			HAL_Delay(3000);
			
			if (error_flag) {
				break;
			}
			
			BSP_LED_Off(LED4);
			current_state = GREEN_FLASHING;
			int i = 0;
			for(i = 0; i < 5 && !error_flag; i++){
				BSP_LED_On(LED4);
				HAL_Delay(300);
				BSP_LED_Off(LED4);
				HAL_Delay(300);
			}
			
			if (error_flag) {
				break;
			}
			
			current_state = YELLOW;
			BSP_LED_On(LED3);
			HAL_Delay(1000);
			BSP_LED_Off(LED3);
		}
    }
```



#### Kommunikation über den SPI-Bus

Die folgende Funktion ermöglicht das senden über den SPI-Bus. Sie teilt sich in drei Teile dem Senden und Empfangen über den Bus. Dem Abwarten bis die Kommunikation abgeschlossen wurde und dem Überprüfen der Antwort. Sobald die Antwort der gesandten Nachricht entspricht  wird die `error_flag`, zuständig für das Schalten zwischen Alarmmodus (Gelb blinkend) und regulärer Ampelsteuerung. und der `error_counter`, zuständig für das Blinken im Alarmmodus und der generellen Fehlertoleranz. 

```c
/* Private functions ---------------------------------------------------------*/
int error_counter = 0;
void send_Current_State(void) {
	if(HAL_SPI_TransmitReceive_IT(&SpiHandle, (uint8_t*)states_s[current_state], (uint8_t *)aRxBuffer, BUFFERSIZE) != HAL_OK){
		/* Transfer error in transmission process */
		Error_Handler();
	}
	while (HAL_SPI_GetState(&SpiHandle) != HAL_SPI_STATE_READY);

	if(Buffercmp((uint8_t*)states_s[current_state], (uint8_t*)aRxBuffer, BUFFERSIZE)){
		/* Transfer error in transmission process */
		error_counter++;
		if(error_counter >= 5){
			Error_Handler();
			BSP_LED_Off(LED4);
			BSP_LED_Off(LED5);
		}
		if(error_counter % 10 == 0){
			BSP_LED_Toggle(LED3);
		}
	} else {
		error_counter = 0;
		error_flag = 0;
	}
}
```

#### Timer Interrupt

In diesem Beispiel wurde die TIM3 Implementierung aus dem Beispiel `TIM_TimeBase` übernommen, dabei sollte angemerkt werden, dass alle Files überprüft werden sollten, um beim Migrieren keinen Sourcecode zu übersehen und der Timer endgültig läuft. Das Objekt `TimHandle` ist zuständig für die Konfiguration des Timers. Neben dem Festlegen des Timers auf die Version `TIM3` wird mit dem Aufruf von `TimHandle.Init.Period` der zeitliche Zyklus festgelegt.

```c
/* Compute the prescaler value to have TIM3 counter clock equal to 10 KHz */
  uwPrescalerValue = (uint32_t) ((SystemCoreClock /2) / 10000) - 1;
  
  /* Set TIMx instance */
  TimHandle.Instance = TIMx;
   
  /* Initialize TIM3 peripheral as follow:
       + Period = 10000 - 1
       + Prescaler = ((SystemCoreClock/2)/10000) - 1
       + ClockDivision = 0
       + Counter direction = Up
  */
  TimHandle.Init.Period = 1000 - 1;
  TimHandle.Init.Prescaler = uwPrescalerValue;
  TimHandle.Init.ClockDivision = 0;
  TimHandle.Init.CounterMode = TIM_COUNTERMODE_UP;
  TimHandle.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if(HAL_TIM_Base_Init(&TimHandle) != HAL_OK)
```

Folgende Methode wird im Zyklus von 100ms unabhängig von der main-Methode über eine Interrupt-Steuerung aufgerufen. Sie ruft daraufhin die zuvor erwähnte `send_current_State()`-Methode auf um den derzeitigen Ampelstatus zu übertragen.

```c
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  send_Current_State();
  //BSP_LED_Toggle(LED6);
}
```



### SPI-Listener am Raspberry

Der Python-Code am Raspberry liest in einer Schleife Daten aus dem SPI-Bus aus und sendet die exakt selben Daten zurück. Gesendet wird aber nur, wenn die empfangenen Daten nicht "high" (= 255) sind. 

[14]

```python
import spidev
import time

if __name__ == "__main__":
    spi_ch = 0
    spi = spidev.SpiDev(0, spi_ch)
    print(str(spi.mode))

    spi.max_speed_hz = 512

    spi.open(0, 0)

    while True:
        check = [255]

        val = spi.readbytes(1)

        if val != check:
            print(str(val))
            spi.writebytes(val)

```



## Quellen

[1] https://www.mbtechworks.com/hardware/raspberry-pi-UART-SPI-I2C.html

[2] https://arduino.stackexchange.com/questions/16348/how-do-you-use-spi-on-an-arduino

[3] https://de.wikipedia.org/wiki/Serial_Peripheral_Interface

[4] https://www.allaboutcircuits.com/technical-articles/spi-serial-peripheral-interface/

[5] https://de.wikipedia.org/wiki/Echtzeitsystem

[6] https://www.antrax.de/watchdog-anwendungen/

[7] http://www.mikrocontroller-programmierung.de/interrupt-prioritaeten-einstellen.php

[8] https://community.arm.com/developer/ip-products/system/b/embedded-blog/posts/cutting-through-the-confusion-with-arm-cortex-m-interrupt-priorities

[9] https://www.motioncontroltips.com/what-is-nested-vector-interrupt-control-nvic/

[10] https://www.quora.com/Why-do-we-need-an-interrupt-in-an-embedded-system

[11] https://www.elektroniknet.de/design-elektronik/embedded/einfuehrung-in-die-rtos-welt-29528.html

[12] https://www.saleae.com/de/

[13] https://de.wikipedia.org/wiki/Logikanalysator

[14] https://github.com/doceme/py-spidev