# Raspy

## SPI-aktivieren

Kommentar beim Parameter `dtparam=spi=on` in `/boot/config.txt` entfernen und Raspy neustarten.

Nach dem Neustart sollte die SPI-Schnittstelle des Raspberries aktiv sein. Mit `lsmod | grep "spidev "` bzw. `ls /dev | grep spi` kann getestet werden.

## Pinbelegung

![](img/RASPY/pins.PNG)

[1]

Bei der Implementierung verwendete Farben:

* WEISS - MOSI
* GRAU - MISO
* SCLK - LILA
* CE0- BLAU



## Quellen

[1] https://pi4j.com/1.2/pins/model-3b-plus-rev1.html

