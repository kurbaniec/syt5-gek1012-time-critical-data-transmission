import spidev
import time

if __name__ == "__main__":
    spi_ch = 0
    spi = spidev.SpiDev(0, spi_ch)
    print(str(spi.mode))

    spi.max_speed_hz = 512

    spi.open(0, 0)

    while True:
        #check = [255, 255, 255, 255, 255, 255, 255, 255]
        check = [255]

        val = spi.readbytes(1)

        if val != check:
            print(str(val))
            spi.writebytes(val)
