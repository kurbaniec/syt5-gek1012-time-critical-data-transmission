/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Uncomment this line to use the board as master, if not it is used as slave */
#define MASTER_BOARD

/* Private variables ---------------------------------------------------------*/
/* SPI handler declaration */
SPI_HandleTypeDef SpiHandle;

typedef enum
{
	RED = 0,
	RED_YELLOW,
	GREEN,
	GREEN_FLASHING,
	YELLOW,
	YELLOW_FLASHING,
} STATE;
char* states_s[] = { "1110", "1101", "0010", "0101", "1000", "0001" };

/* Error-Flag */
int error_flag = 0;

/* Buffer used for saving the current state */
uint8_t current_state = YELLOW_FLASHING;

/* Buffer used for transmission */
uint8_t aTxBuffer[] = "0001";

/* Buffer used for reception */
uint8_t aRxBuffer[BUFFERSIZE];

/* Timer */
TIM_HandleTypeDef TimHandle;
uint32_t uwPrescalerValue = 0;


/* Private function prototypes -----------------------------------------------*/
static void SystemClock_Config(void);
static void Error_Handler(void);
static uint16_t Buffercmp(uint8_t* pBuffer1, uint8_t* pBuffer2, uint16_t BufferLength);

/* Private functions ---------------------------------------------------------*/
int error_counter = 0;
void send_Current_State(void) {
	if(HAL_SPI_TransmitReceive_IT(&SpiHandle, (uint8_t*)states_s[current_state], (uint8_t *)aRxBuffer, BUFFERSIZE) != HAL_OK){
		/* Transfer error in transmission process */
		Error_Handler();
	}
	while (HAL_SPI_GetState(&SpiHandle) != HAL_SPI_STATE_READY);

	if(Buffercmp((uint8_t*)states_s[current_state], (uint8_t*)aRxBuffer, BUFFERSIZE)){
		/* Transfer error in transmission process */
		error_counter++;
		if(error_counter >= 5){
			Error_Handler();
			BSP_LED_Off(LED4);
			BSP_LED_Off(LED5);
		}
		if(error_counter % 10 == 0){
			BSP_LED_Toggle(LED3);
		}
	} else {
		error_counter = 0;
		error_flag = 0;
	}
}

/* Main function -------------------------------------------------------------*/
int main(void)
{
	/* STM32F4xx HAL library initialization:
	   - Configure the Flash prefetch, instruction and Data caches
	   - Configure the Systick to generate an interrupt each 1 msec
	   - Set NVIC Group Priority to 4
	   - Global MSP (MCU Support Package) initialization
	 */
	if(HAL_Init()!= HAL_OK)
	{
		/* Start Conversation Error */
		Error_Handler(); 
	}
	

	/* Configure LED3, LED4, LED5 and LED6 */
	BSP_LED_Init(LED3);  // Yellow
	BSP_LED_Init(LED4);  // Green
	BSP_LED_Init(LED5);  // Red
	BSP_LED_Init(LED6);  // Blue

	/* Configure the system clock to 168 MHz */
	SystemClock_Config();

	/*##-1- Configure the SPI peripheral #######################################*/
	/* Set the SPI parameters */
	SpiHandle.Instance               = SPIx;
	SpiHandle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_256;
	SpiHandle.Init.Direction         = SPI_DIRECTION_2LINES;
	SpiHandle.Init.CLKPhase          = SPI_PHASE_1EDGE;
	SpiHandle.Init.CLKPolarity       = SPI_POLARITY_HIGH;
	SpiHandle.Init.CRCCalculation    = SPI_CRCCALCULATION_DISABLE;
	SpiHandle.Init.CRCPolynomial     = 7;
	SpiHandle.Init.DataSize          = SPI_DATASIZE_8BIT;
	SpiHandle.Init.FirstBit          = SPI_FIRSTBIT_MSB;
	SpiHandle.Init.NSS               = SPI_NSS_SOFT;
	SpiHandle.Init.TIMode            = SPI_TIMODE_DISABLE;
  
#ifdef MASTER_BOARD
	SpiHandle.Init.Mode = SPI_MODE_MASTER;
#else
	SpiHandle.Init.Mode = SPI_MODE_SLAVE;
#endif

	if(HAL_SPI_Init(&SpiHandle) != HAL_OK)
	{
		/* Initialization Error */
		Error_Handler();
	}

#ifdef MASTER_BOARD
	 /*##-1- Configure the TIM peripheral #######################################*/ 
  /* -----------------------------------------------------------------------
    In this example TIM3 input clock (TIM3CLK) is set to 2 * APB1 clock (PCLK1), 
    since APB1 prescaler is different from 1.   
      TIM3CLK = 2 * PCLK1  
      PCLK1 = HCLK / 4 
      => TIM3CLK = HCLK / 2 = SystemCoreClock /2
    To get TIM3 counter clock at 10 KHz, the Prescaler is computed as following:
    Prescaler = (TIM3CLK / TIM3 counter clock) - 1
    Prescaler = ((SystemCoreClock /2) /10 KHz) - 1
       
    Note: 
     SystemCoreClock variable holds HCLK frequency and is defined in system_stm32f4xx.c file.
     Each time the core clock (HCLK) changes, user had to update SystemCoreClock 
     variable value. Otherwise, any configuration based on this variable will be incorrect.
     This variable is updated in three ways:
      1) by calling CMSIS function SystemCoreClockUpdate()
      2) by calling HAL API function HAL_RCC_GetSysClockFreq()
      3) each time HAL_RCC_ClockConfig() is called to configure the system clock frequency  
  ----------------------------------------------------------------------- */  
  
  /* Compute the prescaler value to have TIM3 counter clock equal to 10 KHz */
  uwPrescalerValue = (uint32_t) ((SystemCoreClock /2) / 10000) - 1;
  
  /* Set TIMx instance */
  TimHandle.Instance = TIMx;
   
  /* Initialize TIM3 peripheral as follow:
       + Period = 10000 - 1
       + Prescaler = ((SystemCoreClock/2)/10000) - 1
       + ClockDivision = 0
       + Counter direction = Up
  */
  TimHandle.Init.Period = 1000 - 1;
  TimHandle.Init.Prescaler = uwPrescalerValue;
  TimHandle.Init.ClockDivision = 0;
  TimHandle.Init.CounterMode = TIM_COUNTERMODE_UP;
  TimHandle.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if(HAL_TIM_Base_Init(&TimHandle) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }
  
  /*##-2- Start the TIM Base generation in interrupt mode ####################*/
  /* Start Channel1 */
  if(HAL_TIM_Base_Start_IT(&TimHandle) != HAL_OK)
  {
    /* Starting Error */
    Error_Handler();
  }

/*
	BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_GPIO);
	while (BSP_PB_GetState(BUTTON_KEY) != 1)
	{
	BSP_LED_Toggle(LED3);
	BSP_LED_Toggle(LED4);
	BSP_LED_Toggle(LED5);
	BSP_LED_Toggle(LED6);
	HAL_Delay(300);
	}
	BSP_LED_Off(LED3);
	BSP_LED_Off(LED4);
	BSP_LED_Off(LED5);
	BSP_LED_Off(LED6);
*/	

    while (1)
    {
		while (!error_flag)
		{
			current_state = RED;
			BSP_LED_On(LED5);
			HAL_Delay(3000);
			
			if (error_flag) {
				break;
			}
			
			current_state = RED_YELLOW;
			BSP_LED_On(LED3);
			HAL_Delay(1000);
			
			if (error_flag) {
				break;
			}
			
			BSP_LED_Off(LED3);
			BSP_LED_Off(LED5);
			current_state = GREEN;
			BSP_LED_On(LED4);
			HAL_Delay(3000);
			
			if (error_flag) {
				break;
			}
			
			BSP_LED_Off(LED4);
			current_state = GREEN_FLASHING;
			int i = 0;
			for(i = 0; i < 5 && !error_flag; i++){
				BSP_LED_On(LED4);
				HAL_Delay(300);
				BSP_LED_Off(LED4);
				HAL_Delay(300);
			}
			
			if (error_flag) {
				break;
			}
			
			current_state = YELLOW;
			BSP_LED_On(LED3);
			HAL_Delay(1000);
			BSP_LED_Off(LED3);
		}
    }

#else

	while (1){
		if(HAL_SPI_TransmitReceive_IT(&SpiHandle, (uint8_t*)aTxBuffer, (uint8_t *)aRxBuffer, BUFFERSIZE) != HAL_OK)
		{
			/* Transfer error in transmission process */
			Error_Handler();
		}
		while (HAL_SPI_GetState(&SpiHandle) != HAL_SPI_STATE_READY);
		memcpy(aTxBuffer,aRxBuffer,BUFFERSIZE);
	}
	
#endif
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Error_Handler(void)
{
  error_flag = 1;
}

#ifdef MASTER_BOARD

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  send_Current_State();
  //BSP_LED_Toggle(LED6);
}

#endif


/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 168000000
  *            HCLK(Hz)                       = 168000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 4
  *            APB2 Prescaler                 = 2
  *            HSE Frequency(Hz)              = 8000000
  *            PLL_M                          = 8
  *            PLL_N                          = 336
  *            PLL_P                          = 2
  *            PLL_Q                          = 7
  *            VDD(V)                         = 3.3
  *            Main regulator output voltage  = Scale1 mode
  *            Flash Latency(WS)              = 5
  * @param  None
  * @retval None
  */
static void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;

  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();
  
  /* The voltage scaling allows optimizing the power consumption when the device is 
     clocked below the maximum system frequency, to update the voltage scaling value 
     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  
  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);
 
  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;  
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;  
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5);

  /* STM32F405x/407x/415x/417x Revision Z devices: prefetch is supported  */
  if (HAL_GetREVID() == 0x1001)
  {
    /* Enable the Flash prefetch */
    __HAL_FLASH_PREFETCH_BUFFER_ENABLE();
  }
}

/**
  * @brief  TxRx Transfer completed callback
  * @param  hspi: SPI handle. 
  * @note   This example shows a simple way to report end of Interrupt TxRx transfer, and 
  *         you can add your own implementation. 
  * @retval None
  */
void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi)
{
  /* Turn LED6 off: Transfer in transmission process is correct */
  BSP_LED_Off(LED6);
}

/**
  * @brief  SPI error callbacks
  * @param  hspi: SPI handle
  * @note   This example shows a simple way to report transfer error, and you can
  *         add your own implementation.
  * @retval None
  */
 void HAL_SPI_ErrorCallback(SPI_HandleTypeDef *hspi)
{
  BSP_LED_On(LED6);
}

/**
  * @brief  Compares two buffers.
  * @param  pBuffer1, pBuffer2: buffers to be compared.
  * @param  BufferLength: buffer's length
  * @retval 0  : pBuffer1 identical to pBuffer2
  *         >0 : pBuffer1 differs from pBuffer2
  */
static uint16_t Buffercmp(uint8_t* pBuffer1, uint8_t* pBuffer2, uint16_t BufferLength)
{
  while (BufferLength--)
  {
    if((*pBuffer1) != *pBuffer2)
    {
      return BufferLength;
    }
    pBuffer1++;
    pBuffer2++;
  }

  return 0;
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif
